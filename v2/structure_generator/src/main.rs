use scraper;

use std::fs::File;
use std::io::prelude::*;

use scraper::{Html, Selector};

fn filenameify(s: &str) -> String {
    s
        .to_lowercase()
        .replace("å", "a")
        .replace("ä", "a")
        .replace("ö", "o")
        .replace(" ", "_")
}

fn main() {
    let base_dir = "files";

    // Open the html file
    let mut file = File::open("../blommorStorEva.html").expect("Faield to open HTML");
    let mut contents = String::new();
    file.read_to_string(&mut contents).expect("Failed to read HTML");

    // Parse the HTML
    let document = Html::parse_document(&contents);

    // Generate HTML selectors
    let main_selector = Selector::parse("div.main")
        .expect("Failed to parse selector");
    let div_selector = Selector::parse("div").expect("Failed to parse div selector");
    let header_selector = Selector::parse(".headerContainer > h1").unwrap();
    let img_selector = Selector::parse("img").unwrap();
    let a_selector = Selector::parse("a").unwrap();
    let name_selector = Selector::parse(".imgContainer > div > h2").unwrap();

    // Parse the document
    let main_elem = document.select(&main_selector).next().expect("No main div");


    // Current state
    let mut last_flower: Option<String> = None;
    let mut current_category: Option<String> = None;
    let mut current_ordering: Vec<String> = vec!();
    let mut outer_ordering: Vec<String> = vec!();
    for element in main_elem.select(&div_selector) {
        // On finding a new header
        if let Some(header) = element.select(&header_selector).next() {
            // We found the content of the previous category, save it
            if let Some(category) = current_category {
                // Create a directory with a details file
                let dir_name = format!("{}/{}", base_dir, filenameify(&category));
                let details_file = format!("{}/details.toml", dir_name);
                println!("mkdir -p {}", dir_name);
                println!("echo '[category]' > {}", details_file);
                println!("echo 'name=\"{}\"' >> {}", category, details_file);
                println!("echo 'order = [' >> {}", details_file);
                current_ordering.iter()
                    .for_each(|name| {
                        println!("echo '\"{}\",' >> {}", name, details_file)
                    });
                println!("echo ']' >> {}", details_file);

                outer_ordering.push(filenameify(&category));
            }
            let new_category = header.text().collect::<Vec<_>>().join(" ");

            current_category = Some(new_category);
            current_ordering.clear();
        }
        // On finding a file
        else if let Some(header) = element.select(&name_selector).next() {
            // Extract name information
            let text = header.text().collect::<Vec<_>>();
            let header = text.join(" ");
            let name: String = header.split(",")
                .next()
                .expect("No first element aftersplit")
                .into();
            let location = header.split(",")
                .nth(1);

            // Hack to make sure things aren't duplicated
            if last_flower != Some(name.clone()) {
                last_flower = Some(name.clone());
                current_ordering.push(filenameify(&name));

                let dir_name = filenameify(&format!(
                    "{}/{}/{}",
                    base_dir,
                    current_category.clone().unwrap(),
                    name
                ));


                // Parse image location
                let low_res = element.select(&img_selector)
                    .next().expect("no img")
                    .value()
                    .attr("src")
                    .expect("No src attribute");
                let high_res = element.select(&a_selector)
                    .next().expect("no a")
                    .value()
                    .attr("href")
                    .expect("No href attribute");

                let details_file = format!("{}/details.toml", dir_name);
                let low_dir = format!("{}/low", dir_name);
                let high_dir = format!("{}/high", dir_name);
                let filename = high_res.split("/").last().expect("No file in img filename");

                println!("mkdir -p {}", dir_name);
                println!("mkdir -p {}", low_dir);
                println!("mkdir -p {}", high_dir);
                println!("cp ../{} {}", high_res, high_dir);
                println!("cp ../{} {}", low_res, low_dir);
                println!("echo '[leaf]' > {}", details_file);
                println!("echo 'name = \"{}\"' >> {}", name, details_file);
                println!("echo 'filename = \"{}\"' >> {}", filename, details_file);
                if let Some(location) = location {
                    println!("echo 'location = \"{}\"' >> {}", location.trim(), details_file);
                }
            }
        }
        else {
            println!("========== Warning: weird element ============");
        }
    }
    // Output the last dir
    if let Some(category) = current_category {
        // Create a directory with a details file
        let dir_name = format!("{}/{}", base_dir, filenameify(&category));
        let details_file = format!("{}/details.toml", dir_name);
        println!("mkdir -p {}", dir_name);
        println!("echo '[category]' > {}", details_file);
        println!("echo 'name=\"{}\"' >> {}", category, details_file);
        println!("echo 'order = [' >> {}", details_file);
        current_ordering.iter()
            .for_each(|name| {
                println!("echo '\"{}\",' >> {}", name, details_file)
            });
        println!("echo ']' >> {}", details_file);

        outer_ordering.push(filenameify(&category));
    }

    let root_details = format!("{}/details.toml", base_dir);
    println!("mkdir -p {}", base_dir);
    println!("echo '[category]' > {}", root_details);
    println!("echo 'name = \"root\"' >> {}", root_details);
    println!("echo 'order = [' >> {}", root_details);
    outer_ordering.iter()
        .for_each(|name| {
            println!("echo '\"{}\",' >> {}", name, root_details)
        });
    println!("echo ']' >> {}", root_details);
}
