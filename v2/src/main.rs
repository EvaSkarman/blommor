mod content_tree;
mod html_generator;
mod parse_error;

use std::fs::{self, File};
use std::io::{Write, Read};
use std::path::{Path, PathBuf};
use std::collections::hash_map::HashMap;

use structopt::StructOpt;

use parse_error::{Result, ToParseResult, ErrorKind, ParseError};
use content_tree::{
    Node,
    InnerDeserializeable,
    LeafDeserializeable,
    LegacyLeafDeserializeable
};

fn traverse_dir(dir: &Path) -> Result<Node> {
    // Convert the string to something readable
    let dir_name: String = dir.to_string_lossy().into();

    // Ensure that the path is a directory
    if !dir.is_dir() {
        return Err(ParseError::new(ErrorKind::NotADirectory, &dir_name))
    }


    // Try to find the details file for this inner node
    let details_file_path = dir.join("details.toml");
    let mut details_file = File::open(details_file_path.clone())
        .to_parse_result(&dir_name)?;
    let mut details_content = String::new();
    details_file.read_to_string(&mut details_content).to_parse_result(&dir_name)?;

    let maybe_leaf = LeafDeserializeable::de(&details_content);
    let maybe_inner = InnerDeserializeable::de(&details_content);

    match (maybe_leaf, maybe_inner) {
        (Ok(leaf), Err(_)) => {
            Ok(Node::Leaf(dir_name.into(), leaf.leaf))
        },
        (Err(_), Ok(inner)) => {
            let mut nodes = HashMap::new();
            // Build an inner node using the dirs as nodes
            for entry in fs::read_dir(dir).to_parse_result(&dir_name)? {
                let entry = entry.to_parse_result(&dir_name)?;
                let path = entry.path();

                let local_name = path.file_name().unwrap().to_string_lossy().to_string();

                if path.is_dir() {
                    let node = traverse_dir(&path).to_parse_result(&dir_name)?;
                    nodes.insert(local_name, Some(node));
                }
                else {
                    // Unwrap is safe because we already checked if this is a dir
                    let filename = path.file_name().unwrap().to_string_lossy();
                    if filename != "details.toml" {
                        return Err(ParseError::new(ErrorKind::UnexpectedFile, &dir_name))
                    }
                }
            }

            // Make sure that all the nodes mentioned in the ordering
            // are present
            let missing_nodes = inner.category.order
                .iter()
                .filter(|x| !nodes.contains_key(x.as_str()))
                .map(|k| k.to_string())
                .collect::<Vec<_>>();

            // Make sure that all nodes found are present in the ordering
            let extra_nodes = nodes.keys()
                .filter(|k| !inner.category.order.contains(k))
                .map(|k| k.to_string())
                .collect::<Vec<_>>();

            if !missing_nodes.is_empty() {
                return Err(ParseError::new(ErrorKind::MissingNodes(missing_nodes), &dir_name))
            }
            if !extra_nodes.is_empty() {
                return Err(ParseError::new(ErrorKind::ExtraNodes(extra_nodes), &dir_name))
            }

            // Gather all the children. The unwrap is safe because we already
            // know that there are no extra or missing nodes nodes
            let children = inner.category.order.iter()
                .map(move |k| nodes[k].clone().unwrap())
                .collect::<Vec<_>>();

            Ok(Node::Inner(inner.category, children))
        }
        (Ok(_), Ok(_)) => {
            Err(ParseError::new(ErrorKind::BothLeafAndInner, &dir_name))
        }
        (Err(leaf_err), Err(inner)) => {
            // Try to parse as a legacy leaf
            if let Ok(old) = LegacyLeafDeserializeable::de(&details_content) {
                println!("Successfully decoded legacy leaf");

                let mut details_file = File::create(details_file_path).unwrap();
                let content = old.into_non_legacy().se().unwrap();
                details_file.write_all(content.as_bytes())
                    .unwrap();


                // Recursively call self now that we have updated the content file
                traverse_dir(dir)
            }
            else {
                Err((leaf_err, inner)).to_parse_result(&dir_name)
            }
        }
    }
}

fn base_html(extra_head: &str, rest_html: &str) -> String {
    format!(
     r#"<doctype=html>
        <head>
            <meta charset="utf-8">
            <title>flowers</title>
            <link rel="stylesheet" href="css/style.css">
            {}
        </head>
        <body>
            <div class="main">
            {}
            </div>
        </body>
    "#, extra_head, rest_html)
}

#[derive(StructOpt)]
struct Opt {
    #[structopt()]
    file_dir: PathBuf,
    #[structopt(short)]
    output_file: PathBuf
}

fn main() {
    let opt = Opt::from_args();
    let base_path = opt.file_dir;

    let tree = traverse_dir(&base_path);

    let tree = match tree {
        Ok(tree) => tree,
        Err(e) => panic!("Error: {}", e.pretty_message())
    };

    let extra_pages = tree.traverse(
            &|_| None,
            &|path_to, details| {
                details.extra_content().map(|extra| (PathBuf::from(path_to), extra))
            }
        )
        .into_iter()
        .filter_map(|x| x)
        .collect::<Vec<_>>();


    for (location, (head, body)) in extra_pages {
        let mut full_location = PathBuf::from(location);
        // Create the full path to the directory. Skip 1 because ancestors returns
        // the empty string as the first value
        for dir in full_location.ancestors()
                .collect::<Vec<_>>()
                .iter()
                .rev()
                .skip(1)
        {
            if !dir.exists() {
                fs::create_dir(&dir).expect("Failed to create dir");
            }
        }
        full_location.push("details.html");


        let mut file = File::create(full_location)
            .expect("Failed to create output HTML");
        file.write_all(base_html(&head, &body).as_bytes())
            .expect("Failed to write HTML");
    }

    let mut file = File::create(opt.output_file).expect("Failed to create output HTML");
    file.write_all(base_html("", &html_generator::root_to_html(tree)).as_bytes())
        .expect("Failed to write HTML");
}
