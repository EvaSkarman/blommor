#[derive(Debug)]
pub enum ErrorKind {
    NotADirectory,
    BothLeafAndInner,
    UnexpectedFile,
    MissingNodes(Vec<String>),
    ExtraNodes(Vec<String>),
    // DeserializationError(toml::de::Error),
    DoubleDeserializationError((toml::de::Error, toml::de::Error)),
    IoError(std::io::Error),
    Downstream(Box<ParseError>)
}

impl ErrorKind {
    fn to_string(&self) -> String {
        match self {
            ErrorKind::NotADirectory =>
                format!("Expected a directory but found a file"),
            ErrorKind::BothLeafAndInner =>
                format!("The details file indicates that the node is both a leaf and an inner node"),
            ErrorKind::UnexpectedFile =>
                format!("An inner node contains both a file that is not details.toml or another directory"),
            ErrorKind::MissingNodes(nodes) =>
                format!("Nodes {:?} were mentioned in ordering, but the corresponding directories are not present", nodes),
            ErrorKind::ExtraNodes(nodes) =>
                format!("Nodes {:?} has directories, but are not present in the ordering", nodes),
            ErrorKind::DoubleDeserializationError((leaf, inner)) =>
                format!("Failed to parse toml as a leaf or inner.\n> When parsing as leaf: {:?}\n When parsing as inner: {:?}", leaf, inner),
            ErrorKind::IoError(inner) =>
                format!("IO Error: {:?}", inner),
            ErrorKind::Downstream(inner) =>
                format!("{}", inner.pretty_message())
        }
    }
}

#[derive(Debug)]
pub struct ParseError {
    kind: ErrorKind,
    node: String,
}

impl ParseError {
    pub fn new(kind: ErrorKind, node: &str) -> Self {
        Self {
            kind,
            node: node.into()
        }
    }

    pub fn pretty_message(&self) -> String {
        format!("when parsing node: {:?}:\n{}", self.node, self.kind.to_string())
    }
}

pub type Result<T> = std::result::Result<T, ParseError>;


pub trait ToParseResult<T> {
    fn to_parse_result(self, node: &str) -> Result<T>;
}

impl<T> ToParseResult<T> for Result<T> {
    fn to_parse_result(self, node: &str) -> Result<T> {
        self.map_err(|err| {
            ParseError {
                kind: ErrorKind::Downstream(Box::new(err)),
                node: node.into()
            }
        })
    }
}



// Macro for blanket impl of ErrorKind. Downstream errors must be boxed and
// therefore can not use the macro
macro_rules! impl_to_parse_result {
    ($origin_type:ty, $error_kind:expr) => {
        impl<T> ToParseResult<T> for std::result::Result<T, $origin_type> {
            fn to_parse_result(self, node: &str) -> Result<T> {
                self.map_err(|err| {
                    ParseError {
                        kind: $error_kind(err),
                        node: node.into()
                    }
                })
            }
        }
    }
}

impl_to_parse_result!(
    (toml::de::Error, toml::de::Error),
    ErrorKind::DoubleDeserializationError
);
impl_to_parse_result!(std::io::Error, ErrorKind::IoError);
